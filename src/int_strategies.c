#include "int_strategies.h"
#include <stdio.h>


void print_int(void* element){
	printf("%d", *((int*)element));
}


int int_equal(void* lhs, void* rhs){
	return *((int*)lhs) == *((int*)rhs);
}
