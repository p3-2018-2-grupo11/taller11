#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "lista.h"
#include "int_strategies.h"

void* funcionPtr(void* args);

typedef struct LimitesTDA {
	int izq;
	int dere;
} Limites;

ListaEnlazada* lista = NULL;
int main(int argc, char* argv[]){

	if (argc < 5){

		exit(EXIT_FAILURE);
	} 

	int nhilos = atoi(argv[2]);
	int kitems   = atoi(argv[4]);
	lista                  = Lista_Inicializar();
	pthread_t* thread_ptrs = (pthread_t*) malloc(sizeof(pthread_t) * nhilos);

	for (int i = 0; i < nhilos; i++){

		Limites* args = (Limites*) malloc(sizeof(Limites));
		args->izq   = i * kitems;
		args->dere  = (i + 1) * kitems;	
		pthread_create(thread_ptrs + i, NULL, funcionPtr, (void*) args);

	}

	for (int i = 0; i < nhilos; i++){
		void* return_holder = NULL;
		pthread_join(thread_ptrs[i], return_holder);
	}

	Lista_Mostrar(lista, print_int);

	MemoriaElementos out_elements; 
	Lista_Destruir(lista, &out_elements);

	free(thread_ptrs);

	return 0;
}


void* funcionPtr(void* args){

	Limites* limits = (Limites*) args;

	for (int i = limits->izq; i < limits->dere; i++){
		int* nv = (int*)malloc(sizeof(int));
		*nv     = i;

		Lista_InsertarFin(lista, nv);
	}

	
	int seEncontro = TRUE;

	for (int i = limits->izq; i < limits->dere; i++){
		ElementoLista* busqueda = Lista_Buscar(lista, &i, int_equal);

		if (busqueda == NULL){	
			seEncontro = FALSE;
			break;
		}
	}

	if (seEncontro){
		printf("Encontro todos\n");
	} 
	else printf("No encontro todos\n"); 	

	for (int i = limits->izq; i < limits->dere; i++){
		int* borrado = Lista_Sacar(lista, &i, int_equal);
		free(borrado);
		
	}

	free(limits);

	return NULL;
}






