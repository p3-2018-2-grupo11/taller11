
#include "lista.h"

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>



static ElementoLista* _allocNuevoNodo(void* elemento){

	ElementoLista* nuevo_elemento = 
		(ElementoLista*) malloc(sizeof(ElementoLista));

	if (nuevo_elemento == NULL) return NULL;
	
	nuevo_elemento->elemento  = elemento;	
	nuevo_elemento->siguiente = NULL;
	nuevo_elemento->anterior  = NULL;

	return nuevo_elemento;
}




static void* _freeNodo(ElementoLista* eliber){
	if (eliber == NULL) return NULL;

	void* elemento_ptr     = eliber->elemento;
	eliber->siguiente = NULL;
	eliber->anterior  = NULL;

	free(eliber);

	return elemento_ptr;
}


ListaEnlazada* Lista_Inicializar(){
	ListaEnlazada* lista_salida =
	       	(ListaEnlazada*)(malloc(sizeof(ListaEnlazada)));

	if (lista_salida == NULL) return NULL;

	lista_salida->sem = (sem_t*) malloc(sizeof(sem_t));
	sem_init(lista_salida->sem, 0, 1);

	lista_salida->inicial = NULL;
	return lista_salida;
}	

int Lista_Destruir(ListaEnlazada* listaDestruir, MemoriaElementos* memoria_out){


	if (listaDestruir == NULL) return -1;

	int exito = Lista_SacarTodos(listaDestruir, memoria_out);
	if (exito != 0) return -1;

	free(listaDestruir->sem);
	free(listaDestruir);
	return 0;
}



int Lista_Vacia(ListaEnlazada* lista){
	if (lista == NULL) return -1;

	return lista->inicial == NULL;
}



size_t Lista_Conteo(ListaEnlazada* lista){

	if (lista == NULL) return -1;

	size_t count = 0;

	sem_wait(lista->sem);

	ElementoLista* listptr = lista->inicial;

	while (listptr != NULL) {
		count++;
		listptr = listptr->siguiente;
	}
	sem_post(lista->sem);	

	return count;
}


int Lista_InsertarFin(ListaEnlazada* lista, void* elemento){

	if (lista == NULL) return -1;
	ElementoLista* nuevo_elemento = _allocNuevoNodo(elemento);

	sem_wait(lista->sem);
	
	ElementoLista* listptr = lista->inicial;

	if (listptr == NULL){

		lista->inicial = nuevo_elemento;
	} else {

		while (listptr->siguiente != NULL) {
			listptr = listptr->siguiente;	
		}

		listptr->siguiente     = nuevo_elemento;

		nuevo_elemento->anterior = listptr;
	}

	sem_post(lista->sem);

	return 0;
}


int Lista_InsertarInicio(ListaEnlazada* lista, void* elemento){
	if (lista == NULL) return -1;

	ElementoLista* nuevo_elemento = _allocNuevoNodo(elemento);

	sem_wait(lista->sem);

	
	ElementoLista* inicial_ptr = lista->inicial;
	
	if (inicial_ptr != NULL){
		inicial_ptr->anterior      = nuevo_elemento;
		nuevo_elemento->siguiente  = inicial_ptr;
	}

	lista->inicial = nuevo_elemento;

	sem_post(lista->sem);

	return 0;
}


void* Lista_Sacar(ListaEnlazada* lista, void* elemento, IsEqual is_equal){
	if (lista == NULL) return NULL;

	sem_wait(lista->sem);

	void* return_val = NULL;
	if (lista->inicial == NULL) goto RET;

	ElementoLista* listptr = lista->inicial;

	while (listptr != NULL && !is_equal(listptr->elemento, elemento)){
		listptr = listptr->siguiente;
	}

	if (listptr == NULL) goto RET;

	if (listptr == lista->inicial)
		lista->inicial = listptr->siguiente;

	ElementoLista* anterior  = listptr->anterior;
	if (anterior != NULL)
		anterior->siguiente = listptr->siguiente;

	ElementoLista* siguiente = listptr->siguiente;
	if (siguiente != NULL)
		siguiente->anterior = anterior;

	return_val = _freeNodo(listptr);

RET:
	sem_post(lista->sem);

	return return_val;
}

int Lista_SacarTodos(ListaEnlazada* lista, MemoriaElementos* memoria_out){
	if (lista == NULL) return -1;
	
	memoria_out->N = Lista_Conteo(lista);

	sem_wait(lista->sem);

	int return_val = 0;
	if (memoria_out->N == 0){
		memoria_out->elementos = NULL;
		goto RET;
	}

	memoria_out->elementos = (void**)calloc(memoria_out->N, sizeof(void**));
	if (memoria_out->elementos == NULL){	
		return_val = -1;
		goto RET;
	}

	int i = 0;
	ElementoLista* listptr = lista->inicial;
	while (listptr != NULL) {
		ElementoLista* to_delete     = listptr;
		listptr                    = listptr->siguiente;
		memoria_out->elementos[i++] = _freeNodo(to_delete);
	} 

	lista->inicial = NULL;

RET:
	sem_post(lista->sem);

	return return_val;
}


int Lista_InsertarDespues(ListaEnlazada* lista, void* objetivo, void* nuevo, IsEqual is_equal){

	if (lista == NULL) return -1;


	sem_wait(lista->sem);

	int return_val = 0;

	if (lista->inicial == NULL){
		return_val = -1;
		goto RET;
	}

	ElementoLista* listptr = lista->inicial;

	while (listptr != NULL && !is_equal(listptr->elemento, objetivo)){
		listptr = listptr->siguiente;
	}
	
	if (listptr == NULL){
		return_val = -1;
		goto RET;
	}

	ElementoLista* nvNod = _allocNuevoNodo(nuevo);
	nvNod->anterior      = listptr;

	if (listptr->siguiente != NULL){
		nvNod->siguiente = listptr->siguiente;
		listptr->siguiente->anterior = nvNod;
	}

	listptr->siguiente = nvNod;

RET:
	sem_wait(lista->sem);

	return return_val;
}


int Lista_InsertarAntes(ListaEnlazada* lista, void* objetivo, void* nuevo, IsEqual is_equal){
	if (lista == NULL) return -1;

	sem_wait(lista->sem);

	int return_val = 0;

	if (lista->inicial == NULL){
		return_val = -1;
		goto RET;
	}

	ElementoLista* listptr = lista->inicial;

	while (listptr != NULL && !is_equal(listptr->elemento, objetivo)){
		listptr = listptr->siguiente;
	}
	
	if (listptr == NULL){
		return_val = -1;
		goto RET;
	}

	ElementoLista* nvNod = _allocNuevoNodo(nuevo);
	nvNod->siguiente = listptr;

	if (listptr == lista->inicial){
		lista->inicial = nvNod;
	}	


	if (listptr->anterior != NULL){
		listptr->anterior->siguiente = nvNod;
		nvNod->anterior = listptr->anterior;
	}

	listptr->anterior = nvNod;

RET:
	sem_post(lista->sem);

	return return_val;
}


ElementoLista* Lista_Primero(ListaEnlazada* lista){
	if (lista == NULL) return NULL;
	
	return lista->inicial;
}

ElementoLista* Lista_Ultimo(ListaEnlazada* lista){
	if (lista == NULL) return NULL;

	sem_wait(lista->sem);

	ElementoLista* return_val = NULL;
	if (lista->inicial == NULL){
       		goto RET;	       
	}

	ElementoLista* listptr = lista->inicial;
	while (listptr->siguiente != NULL){
		listptr = listptr->siguiente;
	}

	return_val = listptr;

RET:
	sem_post(lista->sem);

	return return_val;
}


ElementoLista* Lista_Buscar(ListaEnlazada* lista, void* elemento, IsEqual is_equal){
	if (lista == NULL) return NULL;

	sem_wait(lista->sem);

	ElementoLista* return_val = NULL;

	ElementoLista* listptr = lista->inicial;
	while (listptr != NULL) {
		if (is_equal(listptr->elemento, elemento)){
			return_val = listptr;
			goto RET;
		} 

		listptr = listptr->siguiente;
	} 

RET:
	sem_post(lista->sem);

	return return_val;
}

ElementoLista* Lista_Siguiente(ListaEnlazada* lista, void* elemento, IsEqual is_equal){
	if (lista == NULL) return NULL;

	ElementoLista* encontrado_ptr = Lista_Buscar(lista, elemento, is_equal);

	if (encontrado_ptr == NULL) return NULL;
	else return encontrado_ptr->siguiente; 
}


ElementoLista* Lista_Anterior(ListaEnlazada* lista, void* elemento, IsEqual is_equal){
	if (lista == NULL) return NULL;

	ElementoLista* encontrado_ptr = Lista_Buscar(lista, elemento, is_equal);

	if (encontrado_ptr == NULL) return NULL;
	else return encontrado_ptr->anterior; 
}



void Lista_Mostrar(ListaEnlazada* lista, PrintProcedure print){
	if (lista == NULL) return;

	sem_wait(lista->sem);

	ElementoLista* listptr = lista->inicial;

	while (listptr != NULL){
		print(listptr->elemento);
		printf("\n");
		listptr = listptr->siguiente;
	}
	sem_post(lista->sem);
}

void Lista_Mostrar_Full(ListaEnlazada* lista, PrintProcedure print){
	if (lista == NULL) return;
	sem_post(lista->sem);

	ElementoLista* listptr = lista->inicial;

	while (listptr != NULL){
		print(listptr->elemento);
		printf("%s", " <-> ");
		listptr = listptr->siguiente;
	}
		
	sem_post(lista->sem);

	printf("%s", "(NULL)");
}










