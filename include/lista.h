#ifndef _LISTA_H
#define _LISTA_H


#include <stdlib.h>
#include <semaphore.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif




typedef struct ElementoListaTDA {
	void* elemento;
	struct ElementoListaTDA* anterior;
	struct ElementoListaTDA* siguiente;
} ElementoLista;


typedef struct ListaEnlazadaTDA{
	ElementoLista* inicial;
	sem_t* sem;	
} ListaEnlazada;


typedef struct MemoriaElementosTDA {
	size_t N;
	void** elementos;
} MemoriaElementos;


typedef int (*IsEqual)(void* lhs, void* rhs);


typedef void (*PrintProcedure)(void* to_print);


ListaEnlazada* Lista_Inicializar();	


int Lista_Destruir(ListaEnlazada* por_destruir, MemoriaElementos* memoria_out);

int Lista_Vacia(ListaEnlazada* lista);

size_t Lista_Conteo(ListaEnlazada* lista);


int Lista_InsertarFin(ListaEnlazada* lista, void* elemento);

int Lista_InsertarInicio(ListaEnlazada* lista, void* elemento);


int Lista_InsertarDespues(ListaEnlazada* lista, void* objetivo, void* nuevo, IsEqual is_equal);


int Lista_InsertarAntes(ListaEnlazada* lista, void* objetivo, void* nuevo, IsEqual is_equal);	



void* Lista_Sacar(ListaEnlazada* lista, void* elemento, IsEqual is_equal);


int Lista_SacarTodos(ListaEnlazada* lista, MemoriaElementos* memoria_out);


ElementoLista* Lista_Primero(ListaEnlazada* lista);


ElementoLista* Lista_Ultimo(ListaEnlazada* lista);


ElementoLista* Lista_Buscar(ListaEnlazada* lista, void* elemento, IsEqual is_equal);


ElementoLista* Lista_Siguiente(ListaEnlazada* lista, void* elemento, IsEqual is_equal);	


ElementoLista* Lista_Anterior(ListaEnlazada* lista, void* elemento, IsEqual is_equal);

void Lista_Mostrar(ListaEnlazada* lista, PrintProcedure print);
void Lista_Mostrar_Full(ListaEnlazada* lista, PrintProcedure print);


#endif
