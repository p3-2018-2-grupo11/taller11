#ifndef INT_STRATEGIES_H
#define INT_STRATEGIES_H

void print_int(void* element);
int int_equal(void* lhs, void* rhs);

#endif
